-- Replace LAPR3_G000 by your team ID
-- Replace mypassword by your own password
create user LAPR3_G000 identified by mypassword;
grant create session, create table, create view, create procedure, create trigger, create sequence to LAPR3_G000;
alter user LAPR3_G000 quota unlimited on users;