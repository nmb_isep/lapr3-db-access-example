create table Client
(
    id    int
        constraint pk_client primary key,
    name  varchar(200),
    vatnr varchar(10)
);

create table Address
(
    id         int
        constraint pk_address primary key,
    street     varchar(30),
    postalcode varchar(15),
    city       varchar(20)
);

create table ClientAddress
(
    cli_id  int,
    addr_id int,
    constraint pk_clientaddress primary key (cli_id, addr_id)
);